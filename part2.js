const fs = require("fs");

const data = fs.readFileSync("input-part2.txt").toString().trim().split("\n");

let aim = 0;
let coords = {x: 0, y: 0};

const positions = new Map();
positions.set("forward", value => {
  coords.x += value;
  coords.y += (value * aim);
});
positions.set("down", value => aim += value);
positions.set("up", value => aim -= value);

const parseCommand = (x) => {
  const arr = x.split(" ");
  return {
    direction: arr[0],
    value: parseInt(arr[1]),
  };
};
const executeCommand = (a) => {
  const command = positions.get(a.direction);
  command(a.value)
};

data
  .map(parseCommand)
  .map(executeCommand);

console.log(coords);
console.log(coords.x * coords.y);
