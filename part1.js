const fs = require("fs");

const data = fs.readFileSync("input-part1.txt").toString().trim().split("\n");

const positions = new Map();
positions.set("forward", { x: 1, y: 0 });
positions.set("down", { x: 0, y: 1 });
positions.set("up", { x: 0, y: -1 });

const parseDirection = (x) => {
  const arr = x.split(" ");
  return {
    direction: arr[0],
    value: parseInt(arr[1]),
  };
};
const scaleVector = (a) => {
  const vector = positions.get(a.direction);
  return {
    x: vector.x * a.value,
    y: vector.y * a.value,
  };
};
const sumVectors = (v1, v2) => {
  return {
    x: v1.x + v2.x,
    y: v1.y + v2.y,
  };
};

const finalVector = data
  .map(parseDirection)
  .map(scaleVector)
  .reduce(sumVectors);

console.log(finalVector);
console.log(finalVector.x * finalVector.y);
